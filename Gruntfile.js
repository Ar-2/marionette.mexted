var path = require('path')
// var unwrap = require('unwrap')

module.exports = function(grunt) {
  // require('load-grunt-tasks')(grunt);
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    meta: {
      version: '<%= pkg.version %>',
      core_banner: 
        '// Marionette.Mexted\n' +
        '// ----------------------\n' +
        '// v<%= pkg.version %>\n' +
        '//\n' + 
        '// <%= grunt.template.today("dd.mm.yyyy") %>\n' +
        '\n'
    },
    clean: {
      build:    'lib',
      compiled: 'src/js',
      tmp:      'tmp'
    },
    coffee: {
      core: {
        expand: true,
        flatten: false,
        src: ['**/*.coffee'],
        cwd: 'src/coffee',
        dest: 'src/js/',
        ext:  '.js',
        options: {
          // join: true,
          // preserve_dirs: false,
          // base_path: 'src'
        }
      }
    },
    preprocess: {
      core: {
        src: 'src/core.js',
        dest: 'tmp/core.js'
      }
    },
    template: {
      options: {
        data: {
          version: '<%= pkg.version %>'
        }
      },
      core: {
        src: '<%= preprocess.core.dest %>',
        dest: '<%= preprocess.core.dest %>'
      }
    },
    concat: {
      options: {
        banner: '<%= meta.core_banner %>'
      },
      core: {
        src: '<%= preprocess.core.dest %>',
        dest: 'lib/<%= pkg.name %>.js'
      }
    },
    uglify: {
      core: {
        src: '<%= concat.core.dest %>',
        dest: 'lib/<%= pkg.name %>.min.js',
        options: {
          mangle: false
        //   sourceMap
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-coffee');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-preprocess');
  grunt.loadNpmTasks('grunt-template');

  grunt.registerTask('default', 'Compile alias', ['compile']);
  grunt.registerTask('compile', ['coffee']);
  grunt.registerTask('build', 'Build all.', ['clean:build', 'compile', 'preprocess', 'template', 'concat', 'uglify', 'clean:tmp', 'clean:compiled']);
};