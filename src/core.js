(function(root, factory) {

  if (typeof define === 'function' && define.amd) {
    define(['marionette', 'underscore', 'underscore.string'], function(Marionette, _, s) {
      return (root.Mexted = factory(root, Marionette, _, s));
    });
  } else if (typeof exports !== 'undefined') {
    var Marionette = require('Marionette');
    var _ = require('underscore');
    var s = require('underscore.string');
    module.exports = factory(root, Marionette, _, s);
  } else {
    root.Mexted = factory(root, root.Marionette, root._, root.s);
  }

}(this, function(root, Marionette, _, s) {
  'use strict';

  var previousMexted = root.Mexted;

  var Mexted = Marionette.Mexted = {};

  Mexted.VERSION = '<%= version %>';

  Marionette.noConflict = function() {
    root.Mexted = previousMexted;
    return this;
  };

  // @include js/initialize.js
  // @include js/behaviors/triggered.js
  // @include js/behaviors/state.js
  // @include js/models/extensions/nested.js
  // @include js/models/extensions/validation.js
  // @include js/models/model.js
  // @include js/views/base_view.js
  // @include js/views/binding.js
  // @include js/views/item_view.js

  return Mexted;
}));