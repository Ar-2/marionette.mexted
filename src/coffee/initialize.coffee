Marionette.Mexted.Extensions = {}
Marionette.Mexted.Views      = {}
Marionette.Mexted.Behaviors  = {}


Marionette.Mexted.extend = (Model, ext) ->
  return unless ext = Marionette.Mexted.Extensions[ext]
  _.defaults Model.prototype, ext

Backbone.View::addEvents = (events) ->
  @events = _({}).extend(@events, events)
  @delegateEvents()
