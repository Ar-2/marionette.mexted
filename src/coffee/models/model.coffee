# class Mexted.Model extends Backbone.NestedModel
#   timedateFormat: 'L LT'
#   constructor: (attributes, options) ->
#     super(attributes, options)
#     new Mexted.Extensions.Nested(@)
#     new Mexted.Extensions.Validation(@)
Backbone.Model.prototype._setAttribute = (attrs, attr, val, unset) ->
    if unset 
      delete attrs[attr] 
    else 
      attrs[attr] = val

Backbone.Model.prototype.set = (key, val, options) ->
  return @ if key == null
  if (typeof key == 'object') 
    attrs = key
    options = val
  else
    (attrs = {})[key] = val

  options || (options = {})

  return false unless @_validate(attrs, options)

  unset      = options.unset
  silent     = options.silent
  changes    = []
  changing   = @_changing
  @_changing = true

  unless changing
    @_previousAttributes = _.clone(@attributes)
    @changed = {}

  current = @attributes
  changed = @changed
  prev    = @_previousAttributes

  for attr, val of attrs 
    changes.push(attr) unless _.isEqual(current[attr], val)
  
    if (!_.isEqual(prev[attr], val))
      changed[attr] = val;
    else
      delete changed[attr];
    @_setAttribute(current, attr, val, unset)

  @id = @get(@idAttribute) if attrs.hasOwnProperty(@idAttribute)
 

  if (!silent) 
    if (changes.length) 
      @_pending = options;
    for v in changes
      @trigger('set:' + v, @, current[v]);
      @trigger('change:' + v, @, current[v], options);

  return @ if (changing) 
  
  if (!silent) 
    while (@_pending) 
      options = @_pending;
      @_pending = false;
      @trigger('change', @, options);
  
  @_pending = false
  @_changing = false
  @

class Mexted.Model extends Backbone.NestedModel
  toString: -> @escape('name')
  timedateFormat: 'L LT'
  # preinitialize: ->
  constructor: (attributes, options = {}) ->
    # base = @initialize
    # @initialize = ->
    #   @beforeInitExtensions.apply(@, arguments)
    #   @initExtensions(options)
    #   base.apply(@, arguments)
    super(attributes, options)
    @beforeInitExtensions.apply(@, arguments)
    @initExtensions(@options)
  beforeInitExtensions: ->
  initExtensions: (options) ->
    @initialize_nested(options)
    @initialize_validation(options)
  # toJSON: (options = {}) ->
  #   if options.except then _.omit(super, options.except) else super

Mexted.extend(Mexted.Model, 'Validation')
Mexted.extend(Mexted.Model, 'Nested')