((Ext) ->
  getOrRun: (m, v) ->
    return m.apply(v, m) if _.isFunction m
    m
  delegateEvent = (nested, current, event, name) ->
    @stopListening(current, event) if current
    @listenTo nested, event, -> @trigger.apply(@, _.union([name], arguments))
  delegateEvents = (nested, opts) ->
    if opts.events
      current = @[nestedName(opts)]
      for k, v of opts.events
        delegateEvent.call(@, nested, current, k, v)
    nested
  nestedName = (opts, def) ->
    "__#{ opts.name || def }"
  Ext.Nested = {
    getNested: (name) -> 
      return null unless @nested
      n = @nested.fields || @nested
      return n[name] if name
      n
    initialize_nested: (options = {}) ->
      return if !@nested || @__nested__ready || options.nested == false
      @__nested__inject()
      @__nested__init()

    __nested__inject: ->
      proto = @constructor.prototype
      base = proto.toJSON
      proto.toJSON = (options = {}) -> 
        json = base.apply(@, arguments)
        json = _.omit(json, 'id') if options.__nested != false && options.ids == false
        @__nested__toJSON(json || {}, options)

      proto.__nested__props  = {}
      for name, opts of proto.nested.fields || proto.nested
        @__nested__injectField(name, opts)
        ((name, opts) -> 
          proto.__nested__props[opts.name || s.camelize(name)] = {
            configurable: false
            get: -> @[nestedName(opts, name)] || @__nested__initField(name, opts)
            set: (object) -> @[nestedName(opts)] = object
          }
        )(name, opts)
      proto.__nested__inject = -> true
      @

    __nested__injectField: (name, opts) ->
      opts.field ||= name + '_id'
      opts.name  ||= s.camelize(name)
      opts.className ||= s.classify(name)
      opts.toJSON  ||= (model, options = {}) -> model.toJSON(options)
      opts.class   ||= window.get('Models', opts.className) || window.get('Collections', opts.className)
      opts.isModel ||= window.get('Models', opts.className)?
      opts.default ||= if opts.isModel then null else []

      if opts.collection or opts.model
        if opts.isModel
          opts.model ||= (opts) -> 
            return null unless collection = opts.collection.apply(@)
            collection.get(@get(opts.field))
          opts.get ||= opts.model
        else
          opts.get ||= opts.collection
        opts.withJSON  ||= -> false
        opts.setParent ||= false
        opts.updateOn  ||= 'field'
        if opts.get
          opts.set = (name, opts) ->
            # nested = opts.get.call(@, opts)
            delegateEvents.call(@, opts.get.call(@, opts), opts)
      else 
        # opts.class = opts.class() if _.isFunction(opts.class)
        opts.filter ||= (opts) ->
          model = opts.class.prototype.model
          return {} unless model
          filter = {}
          filter[model = model.modelName] = {}
          filter[model]["#{ @constructor.modelName }_id"] = @id
          filter
        opts.options ||= {}
        eventName = name.replace('_', '')
        if (@nested.events != false and opts.events == null) || opts.events != false || opts.events == true
          ename = opts.eventsName || name
          opts.events = { add: "add:#{ ename }", change: "change:#{ ename }", remove: "remove:#{ ename }" }

        if opts.json isnt false
          opts.json ||= "#{ name }_attributes"
          unless _.isFunction(opts.json)
            opts.json = ((name, method) -> 
              return (json, m, options) -> json[name] = method.call(@, m, options, opts)
            )(opts.json, opts.toJSON)

        if opts.method
          opts.method = @[opts.method] unless _.isFunction(opts.method)
        else
          opts.method = (clazz, name, def, opts) -> 
            @get(name) || def

        opts.set = (name, opts) ->
          nested = opts.method.call(@, opts.class, name, opts.default, opts)
          unless nested instanceof Backbone.Collection or nested instanceof Backbone.Model or !nested?
            f = @[nestedName(opts)]
            # clazz = Backbone.getOrCall(opts.class, null, @)
            object = f || new opts.class(null, _.extend @__nested__getOpt(opts, 'options'))
            object.filterFields = @__nested__getOpt(opts, 'filter', {}) unless opts.isModel
            if opts.setParent == true
              object[@constructor.modelName] ||= @
              object.parent ||= @
            (object.reset || object.set).call(object, nested)
            nested = object
            return nested if f
            delegateEvents.call(@, nested, opts)
          if nested
            nested.filterFields = @__nested__getOpt(opts, 'filter', {}) unless opts.isModel
            opts.onInit.call(@, nested) if opts.onInit
          nested

      unless opts.updateOn == false || @nested.updateOn == false
        eName = opts.updateOn || @nested.updateOn || 'sync'
        eName = if eName == 'field' then "set:#{ opts.field || name }" else eName
        opts.updateOn = eName

    __nested__init: ->
      Object.defineProperties @, @__nested__props
    __nested__getOpt: (opts, name, def) ->
      if _.isFunction(opts[name]) then opts[name].call(@, opts) else opts[name] || def
    __nested__initField: (name, opts) ->
      if opts.updateOn
        @on opts.updateOn, -> 
          @__nested__set(name, opts)
      @__nested__set(name, opts)
    nestedResetFields: ->
      for name, opts of @getNested()
        @__nested__set(name, opts) if @[nestedName(opts, name)]
      return @;
    __nested__set: (name, opts) ->
      nName = nestedName(opts) 
      @[nName] = opts.set.call(@, name, opts)

    __nested__toJSON: (json, options) ->
      railsServer = options.railsServer is true

      for field, attr of @getNested()
        m = @[attr.name]
        continue if attr.collection or !m
        continue if getOrRun(attr.withJSON, @) == true
        # continue if attr.withJSON == false
        # continue unless attr.withJSON?.apply(@, m) ? true
        if railsServer
          Object.delete(json, field)
          attr.json.call(@, json, m, options) if attr.json isnt false and attr.nested is true
        else
          # opts = { __nested: attr.json, ids: attr.ids }
          opts = { __nested: attr.json isnt false }
          opts.ids = true if attr.ids
          json[field] = attr.toJSON.call(@, m, _.defaults(opts, options), attr)
      json
  }

)(Mexted.Extensions)
