# ((Ext) ->
#   nestedName = (opts, def) ->
#     "__#{ opts.name || def }"
#   Ext.Nested = {
#     initialize_nested: (options = {}) ->
#       return if !@nested || @__nested__ready || options.nested == false
#       # @__nestedFields = @nested.fields || @nested
#       @__nested__inject()
#       @__nested__init()

#     __nested__inject: ->
#       proto = @constructor.prototype
#       base = proto.toJSON
#       proto.toJSON = (options = {}) -> 
#         json = base.apply(@, arguments)
#         json = _.omit(json, 'id') if options.__nested != false && options.ids == false
#         @__nested__toJSON(json || {}, options)

#       proto.__nested__props  = {}
#       for name, opts of proto.nested.fields || proto.nested
#         @__nested__injectField(name, opts)
#         ((name, opts) -> 
#           proto.__nested__props[opts.name || s.camelize(name)] = {
#             configurable: false
#             get: -> @[nestedName(opts, 'default')] || @__nested__initField(name, opts)
#             set: (object) -> @[nestedName(opts)] = object
#           }
#         )(name, opts)
#       proto.__nested__inject = -> true
#       @

#     __nested__injectField: (name, opts) ->
#       unless opts.updateOn == false || proto.nested.updateOn == false
#         eName = opts.updatedOn || proto.nested.updateOn || 'sync'
#         eName = if eName == 'field' then "set:#{ opts.field || name }" else eName
#         opts.event = eName

#       opts.field ||= name + '_id'
#       opts.name ||= s.camelize(name)
#       opts.className ||= name
#       opts.get  ||= (model, options = {}) -> model.toJSON(options)
#       classified = s.classify opts.className
#       opts.class ||= window.get('Models', classified) || window.get('Collections', classified)
#       opts.isModel = window.get('Models', classified)?
#       opts.default ||= if opts.isModel then null else []

#       if opts.collection or opts.model
#         opts.model ||= (opts) -> 
#           return null unless collection = opts.collection.apply(@)
#           collection.get(@get(opts.name + '_id'))
#         opts.withJSON ||= -> false
#       else 
#         # opts.class = opts.class() if _.isFunction(opts.class)
#         opts.filter ||= (opts) ->
#           model = opts.class.prototype.model
#           return {} unless model
#           filter = {}
#           filter[model = model.modelName] = {}
#           filter[model]["#{ @constructor.modelName }_id"] = @id
#           filter
#         opts.options ||= {}
#         eventName = name.replace('_', '')
#         if (@nested.events != false and opts.events == null) || opts.events != false || opts.events == true
#           ename = opts.eventsName || name
#           opts.events = { add: "add:#{ ename }", change: "change:#{ ename }", remove: "remove:#{ ename }" }

#         if opts.json isnt false
#           opts.json ||= "#{ name }_attributes"
#           unless _.isFunction(opts.json)
#             opts.json = ((name, method) -> 
#               return (json, m, options) -> json[name] = method.call(@, m, options)
#             )(opts.json, opts.get)

#         if opts.method
#           opts.method = @[opts.method] unless _.isFunction(opts.method)
#         else
#           opts.method = (clazz, name, def, collection) -> 
#             @get(name) || def
#       @

#     __nested__init: ->
#       Object.defineProperties @, @__nested__props
#     __nested__getOpt: (opts, name, def) ->
#       if _.isFunction(opts[name]) then opts[name].call(@, opts) else opts[name] || def
#     __nested__initField: (name, opts) ->
#       if opts.event
#         @on opts.event, -> @__nested__set(name, opts)
#       @__nested__set(name, opts)
#     __nested__set: (name, opts) ->
#       nName = nestedName(opts)
#       currentNested = @[nName]
#       delegateEvent = (nested, event, name) ->
#         @stopListening(currentNested, event) if currentNested
#         @listenTo nested, event, -> @trigger.apply(@, _.union([name], arguments))
#       delegateEvents = (nested) ->
#         return unless opts.events
#         for k, v of opts.events
#           delegateEvent.call(@, nested, k, v)
#         nested

#       if opts.model
#         nested = opts.model.call(@, opts)
#         delegateEvents.call(@, nested)
#       else
#         nested = opts.method.call(@, opts.class, name, opts.default, opts.collection)
#         unless nested instanceof Backbone.Collection or nested instanceof Backbone.Model or !nested?
#           if f = currentNested
#             f.filterFields = @__nested__getOpt(opts, 'filter', {}) unless opts.isModel
#             return (f.reset || f.set).call(f, nested) 
#           nested = new opts.class(nested, @__nested__getOpt(opts, 'options'))
#           delegateEvents.call(@, nested)
#         if nested
#           nested.filterFields = @__nested__getOpt(opts, 'filter', {}) unless opts.isModel
#           if opts.setParent == true
#             nested[@constructor.modelName] ||= @
#             nested.parent ||= @
#           # nested.onNestedInit(@) if nested.onNestedInit
#           opts.onInit.call(@, nested) if opts.onInit
#       @[nName] = nested
      
#     __nested__toJSON: (json, options) ->
#       railsServer = options.railsServer is true

#       for field, attr of @nested
#         m = @[nestedName(attr)]
#         continue if attr.collection
#         continue unless m && attr.withJSON?.apply(@, m) ? true
#         if m 
#           if railsServer
#             Object.delete(json, field)
#             attr.json.call(@, json, m, options) if attr.json isnt false
#           else
#             opts = { __nested: attr.json isnt false }
#             opts.ids = true if attr.ids
#             _.defaults opts, options
#             json[field] = attr.get.call(@, m, opts)
#       json
#   }
# )(Mexted.Extensions)
