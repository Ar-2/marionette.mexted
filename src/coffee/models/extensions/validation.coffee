((Ext) ->
  window.parseIntOrNull = (value) ->
    return null if value == null
    return parseInt(value)
  setDeep = (attrs, name, value) ->
    path = name.split('.')
    length = path.length - 1
    if length == 0
      attrs[name] = value
      return attrs
    
    a = attrs
    for n, i in path
      if i == length
        a[n] = value
      else
        a = a[n] ||= {}
    attrs
  Ext.ValidationOnSetMap = {
    'date': (value, name) ->
      @[name] = if value then moment(value) else null
  }
  Ext.Validation = {
    __validation__presetMethod: (attrs) -> return attrs
    __validation__onSetMethod: -> 
    initialize_validation: (options = {}) ->
      return if !@validation || options.validation == false

      @__validation__presetMethod = @__validation__preset
      @__validation__onSetMethod  = @__validation__onSetAttr
      @__validation__inject()
      @__validation__set()
      @

    __validation__inject: ->
      validate = @_validate
      @_validate = (attrs, opts) => validate.call(@, @__validation__presetMethod(attrs, opts), opts)

      set = @_setAttribute
      @_setAttribute = (attrs, attr) => 
        set.apply(@, arguments)
        @__validation__onSetMethod(attr, attrs[attr])
    __validation__fields: ->
      _.union(_.keys(@validation), _.keys(@attributes))
    __validation__set: ->
      for name in @__validation__fields()
        continue if name == 'id'
        opts = @validation[name] ||= {}
        if !opts.preset && name.endsWith('_id')
          opts.preset = parseIntOrNull

        if opts.onSet && _.isString(opts.onSet)
          opts.onSet = Ext.ValidationOnSetMap[opts.onSet]
        if opts.set && (opts.dependsOn || opts.setOn)
          @__validation__setDependsOn(name, opts)
          events = (opts.dependsOn || []).map((v) -> "change:#{ v }").join(' ') + ' ' + (opts.setOn || []).map((v) -> "#{ v }").join(' ')
          ( (name, opts) => @on(events, => @__validation__setDependsOn(name, opts, arguments)) )(name, opts)

        value = @get(name)
        @__validation__presetAttr(@attributes, opts, name, value)
        @__validation__onSetAttr(name, value, opts)
      @

    __validation__onSetAttr: (name, value, opts) ->
      opts ||= @validation[name] || {}
      return unless opts.onSet
      opts.onSet.call(@, value, name)
    __validation__onSetAttrs: (attrs) ->
      for name, value of attrs
        @__validation__onSetAttr(name, value)
      @
    __validation__setDependsOn: (name, opts, args = []) ->
      @set name, opts.set.call(@, args)
    __validation__presetAttr: (attrs, opts, name, value, key = '') ->
      return @ if !opts || opts.preset == false
      if opts && opts.preset
        setDeep attrs, name, if value == null then value else opts.preset.call(@, value)
      else if _.isObject(value) && !_.isArray(value)
        setDeep attrs, name, @__validation__preset(value, opts, key + name + '.')
      @
    __validation__preset: (attrs, opts, key = '') ->
      return attrs if opts and opts.preset is false
      for name, value of attrs
        @__validation__presetAttr(attrs, @validation[key + name], name, value, key)
      attrs
    preset: (key, val) ->
      if (typeof key == 'object') 
        attrs = key
      else
        (attrs = {})[key] = val

      if @validation 
        for k, v of attrs
          @__validation__presetAttr(attrs, @validation[k], k, v)
      attrs
  }
)(Mexted.Extensions)