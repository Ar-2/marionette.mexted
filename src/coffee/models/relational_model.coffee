# class Relation
#   klass: null,
#   name: null,
#   options: {
#     updateOn: 'change'
#     nested: true
#   }
#   initialize: (klass, name, opts) ->
#     @klass   = klass
#     @name    = name
#     @options = opts
#     @options.name ||= s.camelize(name)

#   delegateEvent = (object, nested, current, event, name) ->
#     object.stopListening(current, event) if current
#     object.listenTo nested, event, -> object.trigger.apply(object, _.union([name], arguments))
#   delegateEvents = (object, nested) ->
#     if @options.events
#       current = @getNested(object)
#       for k, v of @options.events
#         @delegateEvent.call(object, nested, current, k, v)
#     nested
#   getNestedName: -> "__#{ @options.name || @name }"
#   getNested: (object) -> object[@getNestedName()]
#   getNestedOptions: (object) -> @options.options

#   initNestedFor: (object) ->
#     object[@getNestedName()] = @createNested(object)

#   onNestedCreate: (nested, object) ->
#     if opts.setParent != false
#       nested[@constructor.modelName] ||= object
#       nested.parent ||= @
#     event = if @options.updateOn == 'field' then "change:#{ @options.name }" else @options.updateOn
#     @object.listenTo object, event, => @updateNested(nested)
#     @updateNested(nested)
#     @options.onInit.call(object, nested) if @options.onInit()
#   createNested: (object) ->
#     clazz = @getClass(object)
#     @onNestedCreate(r = new clazz(object.get(@name), @getNestedOptions(object))
#     r
#   updateNested: (object, nested) ->
#     nested.reset(object.get(@name))

#   toJSON: (object) -> @getNested(object)

# class HasMany extends Relation
#   default: []
#   options: {
#     filter: (nested, relation) ->
#       model = relation.getClass(@)
#       return {} unless model
#       filter = {}
#       filter[model = model.modelName] = {}
#       filter[model]["#{ @constructor.modelName }_id"] = @id
#       filter
#   }
#   initialize: ->
#     super
#     if options.events == null || @options.events == true
#       ename = opts.eventsName || name
#       @options.events = { add: "add:#{ ename }", change: "change:#{ ename }", remove: "remove:#{ ename }" }
#   updateNested: (object, nested) ->
#     nested.reset(object.get(@name))
#   onNestedUpdate: (nested, object) ->
#     nested.filterFields = Backbone.getOrCall(@options.filter, [nested, @], object) || {}
#     nested.reset(object.get(@name))

# class HasOne extends Relation
#   options: {
#     collection: null
#     model: null
#   }
#   initialize: (class, name, options) ->
#     super
#     @options.field ||= name + 'id'
#     if @options.model or @options.collection
#       @options.model ||= (opts) -> 
#         return null unless collection = opts.collection.apply(@)
#         collection.get(@get(opts.field))
#       @options.get ||= opts.model
#       # @options.withJSON  ||= -> false
#       @options.setParent ||= false
#       @options.updateOn  ||= 'field'
#     if @options.get
#     @setNested = ->
#       nested = opts.get.call(@, opts)
#       delegateEvents.call(@, nested, opts)
#   # else 
#   onNestedUpdate: (nested, object) ->
#     nested.set(object.get(@key))

# class Mexted.Model extends Backbone.RelationalModel
#   @_relations: null
#   initialize: ->
#     for name, rel in @_relations
#       rel.getInstance(@)
#     @initRelational()
#   initRelational: ->
#     @constructor.prototype.initRelational = -> 

#     @_relations = []
#     for name, opts of @hasOne
#       @_relations.push new HasOne(@constructor, name, opts)
#     for name, opts of @hasMany
#       @_relations.push new HasMany(@constructor, name, opts)
#     @constructor.prototype._relations = @_relations
#   get: ->
#     if 
#   set: ->
#   toJSON: ->