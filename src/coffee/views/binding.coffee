# Mexted.attachBindings = (model, view, options) ->
#   # view.bindings = new 
#   new Mexted.Bindings(model, view, options)
Mexted.attachBindingsToView = (View) ->
  _.defaults View.prototype, Mexted.Bindings
Mexted.BindingsGetMap = {
  'nested': (args) -> 
    name = s.camelize(args.field.slice(0, -3))
    return args.model[name].toString() if args.model[name]
    null
  'default': (args) -> args.model.escape(args.field)
}
Mexted.BindingsOnSetMap = {
  'toggle': (el, args) -> if args.value then el.show() else el.hide()
}
Mexted.Bindings = {
  bindings: {}
  field: (field, tag = 'div', options = {}) ->
    options['data-attr'] ||= field
    tags = options.map((k, v) -> "#{ k }='#{ v }'").join(' ')
    "<#{ tag } #{ tags }>#{ @model.escape(field) }</#{ tag }>"
  # __bindings__getField: (args) ->
  #   args.model.escape(args.field)
  __bindings__setCommonBinding: (field) ->
    binding = @bindings[field] ||= {}
    return if binding.inited
    if binding.get != false && !binding.get?
      binding.get = if field.endsWith('_id') then 'nested' else 'default'
    binding.get = Mexted.BindingsGetMap[binding.get] || Mexted.BindingsGetMap['default'] if _.isString(binding.get)

    binding.el   ||= "[data-field='#{ field }'], [data-attr='#{ field }']"
    binding.type ||= 'innerHTML'
    if binding.onSet && _.isString(binding.onSet)
      binding.onSet = Mexted.BindingsOnSetMap[binding.onSet]
    for e in binding.events || []
      @listenTo @model, e, (value) => @bindingsUpdateField(field)
    binding.inited = true
    return binding
  __bindings__setBinding: (field, binding) ->
    b = @__bindings[field] = _.clone(binding)
    b.$el = @$(b.el)
  __bindings__setBindings: ->
    @__bindings__setBinding(f, b) for f, b of @bindings
    @bindingsShowFields(@__bindings)
  # getBindingEl: (field) ->
  #   @__bindings[field].$el if @__bindings[field]
  bindingsInit: ->
    return if @bindings == false
    @__bindings = {}
    # unless @bindings.applied
    @__bindings__setCommonBinding(f) for f, v of @model.attributes
    @__bindings__setCommonBinding(f) for f, v of @bindings
      # @bindings.applied = true

    @listenTo @,      'render', @__bindings__setBindings
    @listenTo @model, 'change', @bindingsUpdateFields

  bindingsUpdateField: (field, value) ->
    b = @__bindings[field]
    if b 
      args = { $el: b.$el, model: @model, field: field, value: value || @model.get(field) }
      b.$el.prop(b.type, b.get.call(@, args)) if b.get && b.$el.length > 0 
      b.onSet.call(@, b.$el, args) if b.onSet
    @

  bindingsShowFields: (fields) ->
    for f, v of fields
      @bindingsUpdateField(f)
    @
  bindingsUpdateFields: (model, data) ->
    @triggerMethod('bindings:beforeUpdate', arguments)
    @bindingsShowFields(model.changed, true)
    @
}

class Mexted.Behaviors.Bindings extends Marionette.Behavior
  defaults: {
    attachAttributes: true
  }
  events: 
    'binding:field': 'showField',
    'binding:fields': 'showFields'
  constructor: ->
    super
    @view.__bindings = @__bindings = {}
    @bindings = @getOption('bindings') || @view.bindings
    return if @bindings == false
    @bindings ||= {}
    @view.getBindingEl ||= (field) -> @__bindings[field].$el if @__bindings[field]

    @on 'before:render', @onBeforeRenderBinding
    @on 'render', @onRenderBinding

  onBeforeRenderBinding: ->
    @listenTo @view.model, 'change', @onModelChange

    @initBinding(f) for f, v of @view.model.attributes if @getOption('attachAttributes') == true
    @initBinding(f) for f, v of @bindings
  onRenderBinding: ->
    @setBindings()

  initBinding: (field) ->
    binding = @bindings[field] ||= {}
    unless binding.inited
      if binding.get != false && !binding.get?
        binding.get = if field.endsWith('_id') then 'nested' else 'default'
      binding.get = Mexted.BindingsGetMap[binding.get] || Mexted.BindingsGetMap['default'] if _.isString(binding.get)

      binding.el   ||= "[data-field='#{ field }'], [data-attr='#{ field }']"
      binding.type ||= 'innerHTML'
      if binding.onSet && _.isString(binding.onSet)
        binding.onSet = Mexted.BindingsOnSetMap[binding.onSet]
      binding.inited = true
    for e in binding.events || []
      @listenTo @view.model, e, (value) => @showField(field)
    return binding
  setBinding: (field, binding) ->
    b = @__bindings[field] = _.clone(binding)
    b.$el = @view.$(b.el)
  setBindings: ->
    @setBinding(f, b) for f, b of @bindings
    @showFields(@__bindings)
    # @bindingsShowFields(@__bindings)
  showField: (field, value) ->
    b = @__bindings[field]
    if b 
      args = { $el: b.$el, model: @view.model, field: field, value: value || @view.model.get(field) }
      b.$el.prop(b.type, b.get.call(@view, args)) if b.get && b.$el.length > 0 
      b.onSet.call(@view, b.$el, args) if b.onSet
    @
  showFields: (fields) ->
    for f, v of fields
      @showField(f)
    @

  onModelChange: ->
    @view.triggerMethod('bindings:beforeUpdate', arguments)
    @showFields(model.changed)
    # @__bindings__setBindings
    
