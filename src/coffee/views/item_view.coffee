((Views, Marionette) ->
  class Views.ItemView extends Marionette.ItemView
    bindings: {}
    attributes: ->
      name: @model.get('id') || 'new'
      cid:  @model.cid
    constructor: (options) ->
      @behaviors ||= {}
      @behaviors.TriggeredItem ||= {}
      @behaviors.Bindings ||= {}
      super
      # @bindingsInit()
    # remove: ->
    #   @$el.fadeOut => super()

    onBindingsBeforeUpdate: ->
      @showFading()
    showFading: (className = 'updated') ->
      @$el.removeClass(className).addClass(className)
      @$el.on 'webkitAnimationEnd oanimationend msAnimationEnd animationend', =>
        @$el.removeClass(className)

    actions: (options = {}, actions = []) ->
      options.wrapper = 'td' if !options.hasOwnProperty('wrapper') && @tagName == 'tr' 
      Helpers.actions(@model, options, _.union(actions, (@viewActions?() ? @viewActions) || []))
  # Mexted.attachBindingsToView(Views.ItemView)

  Mexted.Views.ItemView = Views.ItemView
)(Mexted, Marionette)