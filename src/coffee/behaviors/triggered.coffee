((Behaviors, Behavior) -> 
  class Behaviors.TriggeredItem extends Behavior
    defaults: {
      preventDefaultClick: false
      # clickEvent: 'click'
      clickEvent: false
      actions: true
    }
    initTriggers: ->
      @modelName ||= @options.modelName || @view.options.model.constructor.modelName || s.underscored(@view.options.model.constructor.name)
      @triggers ||= {}
      # @triggers['click .show_action']    = @modelName + ':show'
      # @triggers['click .edit_action']    = @modelName + ':edit'
      # @triggers['click .destroy_action'] = @modelName + ':destroy'
      if @options.actions
        @triggers['click [name="show"]']    = @modelName + ':show'
        @triggers['click [name="edit"]']    = @modelName + ':edit'
        @triggers['click [name="destroy"]'] = @modelName + ':destroy'
      if @options.clickEvent
        @triggers[@options.clickEvent || 'click'] = { event: @modelName + ':clicked', preventDefault: @options.preventDefaultClick }
    constructor: (options, view) ->
      super
      @initTriggers()
)(Mexted.Behaviors, Marionette.Behavior)