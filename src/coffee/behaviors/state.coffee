((Behaviors, Behavior) -> 
  class Behaviors.StateItem extends Behavior
    defaults: {
      activeClass: 'info'
    }
    constructor: (options, view) ->
      activeClass = options.activeClass
      view._active = false
      view.setActive = (state) ->
        @_active = if state instanceof Boolean then state else !@_active
        if @_active
          @$el.addClass activeClass
        else
          @$el.removeClass activeClass
        @_active
      view.isActive = -> @_active
      super
    onClicked: ->
      @view.setActive()
)(Mexted.Behaviors, Marionette.Behavior)