// Marionette.Mexted
// ----------------------
// v0.1.2
//
// 16.12.2016

(function(root, factory) {

  if (typeof define === 'function' && define.amd) {
    define(['marionette', 'underscore', 'underscore.string'], function(Marionette, _, s) {
      return (root.Mexted = factory(root, Marionette, _, s));
    });
  } else if (typeof exports !== 'undefined') {
    var Marionette = require('Marionette');
    var _ = require('underscore');
    var s = require('underscore.string');
    module.exports = factory(root, Marionette, _, s);
  } else {
    root.Mexted = factory(root, root.Marionette, root._, root.s);
  }

}(this, function(root, Marionette, _, s) {
  'use strict';

  var previousMexted = root.Mexted;

  var Mexted = Marionette.Mexted = {};

  Mexted.VERSION = '0.1.2';

  Marionette.noConflict = function() {
    root.Mexted = previousMexted;
    return this;
  };

  (function() {
    Marionette.Mexted.Extensions = {};
  
    Marionette.Mexted.Views = {};
  
    Marionette.Mexted.Behaviors = {};
  
    Marionette.Mexted.extend = function(Model, ext) {
      if (!(ext = Marionette.Mexted.Extensions[ext])) {
        return;
      }
      return _.defaults(Model.prototype, ext);
    };
  
    Backbone.View.prototype.addEvents = function(events) {
      this.events = _({}).extend(this.events, events);
      return this.delegateEvents();
    };
  
  }).call(this);
  
  (function() {
    var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
      hasProp = {}.hasOwnProperty;
  
    (function(Behaviors, Behavior) {
      return Behaviors.TriggeredItem = (function(superClass) {
        extend(TriggeredItem, superClass);
  
        TriggeredItem.prototype.defaults = {
          preventDefaultClick: false,
          clickEvent: false,
          actions: true
        };
  
        TriggeredItem.prototype.initTriggers = function() {
          this.modelName || (this.modelName = this.options.modelName || this.view.options.model.constructor.modelName || s.underscored(this.view.options.model.constructor.name));
          this.triggers || (this.triggers = {});
          if (this.options.actions) {
            this.triggers['click [name="show"]'] = this.modelName + ':show';
            this.triggers['click [name="edit"]'] = this.modelName + ':edit';
            this.triggers['click [name="destroy"]'] = this.modelName + ':destroy';
          }
          if (this.options.clickEvent) {
            return this.triggers[this.options.clickEvent || 'click'] = {
              event: this.modelName + ':clicked',
              preventDefault: this.options.preventDefaultClick
            };
          }
        };
  
        function TriggeredItem(options, view) {
          TriggeredItem.__super__.constructor.apply(this, arguments);
          this.initTriggers();
        }
  
        return TriggeredItem;
  
      })(Behavior);
    })(Mexted.Behaviors, Marionette.Behavior);
  
  }).call(this);
  
  (function() {
    var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
      hasProp = {}.hasOwnProperty;
  
    (function(Behaviors, Behavior) {
      return Behaviors.StateItem = (function(superClass) {
        extend(StateItem, superClass);
  
        StateItem.prototype.defaults = {
          activeClass: 'info'
        };
  
        function StateItem(options, view) {
          var activeClass;
          activeClass = options.activeClass;
          view._active = false;
          view.setActive = function(state) {
            this._active = state instanceof Boolean ? state : !this._active;
            if (this._active) {
              this.$el.addClass(activeClass);
            } else {
              this.$el.removeClass(activeClass);
            }
            return this._active;
          };
          view.isActive = function() {
            return this._active;
          };
          StateItem.__super__.constructor.apply(this, arguments);
        }
  
        StateItem.prototype.onClicked = function() {
          return this.view.setActive();
        };
  
        return StateItem;
  
      })(Behavior);
    })(Mexted.Behaviors, Marionette.Behavior);
  
  }).call(this);
  
  (function() {
    (function(Ext) {
      var delegateEvent, delegateEvents, nestedName;
      delegateEvent = function(nested, current, event, name) {
        if (current) {
          this.stopListening(current, event);
        }
        return this.listenTo(nested, event, function() {
          return this.trigger.apply(this, _.union([name], arguments));
        });
      };
      delegateEvents = function(nested, opts) {
        var current, k, ref, v;
        if (opts.events) {
          current = this[nestedName(opts)];
          ref = opts.events;
          for (k in ref) {
            v = ref[k];
            delegateEvent.call(this, nested, current, k, v);
          }
        }
        return nested;
      };
      nestedName = function(opts, def) {
        return "__" + (opts.name || def);
      };
      return Ext.Nested = {
        initialize_nested: function(options) {
          if (options == null) {
            options = {};
          }
          if (!this.nested || this.__nested__ready || options.nested === false) {
            return;
          }
          this.__nested__inject();
          return this.__nested__init();
        },
        __nested__inject: function() {
          var base, fn, name, opts, proto, ref;
          proto = this.constructor.prototype;
          base = proto.toJSON;
          proto.toJSON = function(options) {
            var json;
            if (options == null) {
              options = {};
            }
            json = base.apply(this, arguments);
            if (options.__nested !== false && options.ids === false) {
              json = _.omit(json, 'id');
            }
            return this.__nested__toJSON(json || {}, options);
          };
          proto.__nested__props = {};
          ref = proto.nested.fields || proto.nested;
          fn = function(name, opts) {
            return proto.__nested__props[opts.name || s.camelize(name)] = {
              configurable: false,
              get: function() {
                return this[nestedName(opts, name)] || this.__nested__initField(name, opts);
              },
              set: function(object) {
                return this[nestedName(opts)] = object;
              }
            };
          };
          for (name in ref) {
            opts = ref[name];
            this.__nested__injectField(name, opts);
            fn(name, opts);
          }
          proto.__nested__inject = function() {
            return true;
          };
          return this;
        },
        __nested__injectField: function(name, opts) {
          var eName, ename, eventName;
          opts.field || (opts.field = name + '_id');
          opts.name || (opts.name = s.camelize(name));
          opts.className || (opts.className = s.classify(name));
          opts.toJSON || (opts.toJSON = function(model, options) {
            if (options == null) {
              options = {};
            }
            return model.toJSON(options);
          });
          opts["class"] || (opts["class"] = window.get('Models', opts.className) || window.get('Collections', opts.className));
          opts.isModel || (opts.isModel = window.get('Models', opts.className) != null);
          opts["default"] || (opts["default"] = opts.isModel ? null : []);
          if (opts.collection || opts.model) {
            if (opts.isModel) {
              opts.model || (opts.model = function(opts) {
                var collection;
                if (!(collection = opts.collection.apply(this))) {
                  return null;
                }
                return collection.get(this.get(opts.field));
              });
              opts.get || (opts.get = opts.model);
            } else {
              opts.get || (opts.get = opts.collection);
            }
            opts.withJSON || (opts.withJSON = function() {
              return false;
            });
            opts.setParent || (opts.setParent = false);
            opts.updateOn || (opts.updateOn = 'field');
            if (opts.get) {
              opts.set = function(name, opts) {
                var nested;
                nested = opts.get.call(this, opts);
                return delegateEvents.call(this, nested, opts);
              };
            }
          } else {
            opts.filter || (opts.filter = function(opts) {
              var filter, model;
              model = opts["class"].prototype.model;
              if (!model) {
                return {};
              }
              filter = {};
              filter[model = model.modelName] = {};
              filter[model][this.constructor.modelName + "_id"] = this.id;
              return filter;
            });
            opts.options || (opts.options = {});
            eventName = name.replace('_', '');
            if ((this.nested.events !== false && opts.events === null) || opts.events !== false || opts.events === true) {
              ename = opts.eventsName || name;
              opts.events = {
                add: "add:" + ename,
                change: "change:" + ename,
                remove: "remove:" + ename
              };
            }
            if (opts.json !== false) {
              opts.json || (opts.json = name + "_attributes");
              if (!_.isFunction(opts.json)) {
                opts.json = (function(name, method) {
                  return function(json, m, options) {
                    return json[name] = method.call(this, m, options);
                  };
                })(opts.json, opts.toJSON);
              }
            }
            if (opts.method) {
              if (!_.isFunction(opts.method)) {
                opts.method = this[opts.method];
              }
            } else {
              opts.method = function(clazz, name, def, collection) {
                return this.get(name) || def;
              };
            }
            opts.set = function(name, opts) {
              var f, name1, nested, object;
              nested = opts.method.call(this, opts["class"], name, opts["default"], opts.collection);
              if (!(nested instanceof Backbone.Collection || nested instanceof Backbone.Model || (nested == null))) {
                f = this[nestedName(opts)];
                object = f || new opts["class"](null, _.extend(this.__nested__getOpt(opts, 'options')));
                if (!opts.isModel) {
                  object.filterFields = this.__nested__getOpt(opts, 'filter', {});
                }
                if (opts.setParent === true) {
                  object[name1 = this.constructor.modelName] || (object[name1] = this);
                  object.parent || (object.parent = this);
                }
                (object.reset || object.set).call(object, nested);
                nested = object;
                if (f) {
                  return nested;
                }
                delegateEvents.call(this, nested, opts);
              }
              if (nested) {
                if (!opts.isModel) {
                  nested.filterFields = this.__nested__getOpt(opts, 'filter', {});
                }
                if (opts.onInit) {
                  opts.onInit.call(this, nested);
                }
              }
              return nested;
            };
          }
          if (!(opts.updateOn === false || this.nested.updateOn === false)) {
            eName = opts.updatedOn || this.nested.updateOn || 'sync';
            eName = eName === 'field' ? "set:" + (opts.field || name) : eName;
            return opts.updateOn = eName;
          }
        },
        __nested__init: function() {
          return Object.defineProperties(this, this.__nested__props);
        },
        __nested__getOpt: function(opts, name, def) {
          if (_.isFunction(opts[name])) {
            return opts[name].call(this, opts);
          } else {
            return opts[name] || def;
          }
        },
        __nested__initField: function(name, opts) {
          if (opts.updateOn) {
            this.on(opts.updateOn, function() {
              return this.__nested__set(name, opts);
            });
          }
          return this.__nested__set(name, opts);
        },
        __nested__set: function(name, opts) {
          var nName;
          nName = nestedName(opts);
          return this[nName] = opts.set.call(this, name, opts);
        },
        __nested__toJSON: function(json, options) {
          var attr, field, m, opts, railsServer, ref, ref1, ref2;
          railsServer = options.railsServer === true;
          ref = this.nested.fields || this.nested;
          for (field in ref) {
            attr = ref[field];
            m = this[attr.name];
            if (attr.collection || !m) {
              continue;
            }
            if (!((ref1 = (ref2 = attr.withJSON) != null ? ref2.apply(this, m) : void 0) != null ? ref1 : true)) {
              continue;
            }
            if (railsServer) {
              Object["delete"](json, field);
              if (attr.json !== false) {
                attr.json.call(this, json, m, options);
              }
            } else {
              opts = {
                __nested: attr.json !== false
              };
              if (attr.ids) {
                opts.ids = true;
              }
              _.defaults(opts, options);
              json[field] = attr.toJSON.call(this, m, opts);
            }
          }
          return json;
        }
      };
    })(Mexted.Extensions);
  
  }).call(this);
  
  (function() {
    (function(Ext) {
      var setDeep;
      setDeep = function(attrs, name, value) {
        var a, i, j, len, length, n, path;
        path = name.split('.');
        length = path.length - 1;
        if (length === 0) {
          attrs[name] = value;
          return attrs;
        }
        a = attrs;
        for (i = j = 0, len = path.length; j < len; i = ++j) {
          n = path[i];
          if (i === length) {
            a[n] = value;
          } else {
            a = a[n] || (a[n] = {});
          }
        }
        return attrs;
      };
      Ext.ValidationOnSetMap = {
        'date': function(value, name) {
          return this[name] = value ? moment(value) : null;
        }
      };
      return Ext.Validation = {
        __validation__presetMethod: function(attrs) {
          return attrs;
        },
        __validation__onSetMethod: function() {},
        initialize_validation: function(options) {
          if (options == null) {
            options = {};
          }
          if (!this.validation || options.validation === false) {
            return;
          }
          this.__validation__presetMethod = this.__validation__preset;
          this.__validation__onSetMethod = this.__validation__onSetAttr;
          this.__validation__inject();
          this.__validation__set();
          return this;
        },
        __validation__inject: function() {
          var set, validate;
          validate = this._validate;
          this._validate = (function(_this) {
            return function(attrs, opts) {
              return validate.call(_this, _this.__validation__presetMethod(attrs, opts), opts);
            };
          })(this);
          set = this._setAttribute;
          return this._setAttribute = (function(_this) {
            return function(attrs, attr) {
              set.apply(_this, arguments);
              return _this.__validation__onSetMethod(attr, attrs[attr]);
            };
          })(this);
        },
        __validation__set: function() {
          var base, events, j, len, name, opts, ref, value;
          ref = _.union(_.keys(this.validation, _.keys(this.attributes)));
          for (j = 0, len = ref.length; j < len; j++) {
            name = ref[j];
            if (name === 'id') {
              continue;
            }
            opts = (base = this.validation)[name] || (base[name] = {});
            if (!opts.preset && name.endsWith('_id')) {
              opts.preset || (opts.preset = parseInt);
            }
            if (opts.onSet && _.isString(opts.onSet)) {
              opts.onSet = Ext.ValidationOnSetMap[opts.onSet];
            }
            if (opts.set && (opts.dependsOn || opts.setOn)) {
              this.__validation__setDependsOn(name, opts);
              events = (opts.dependsOn || []).map(function(v) {
                return "change:" + v;
              }).join(' ') + ' ' + (opts.setOn || []).map(function(v) {
                return "" + v;
              }).join(' ');
              ((function(_this) {
                return function(name, opts) {
                  return _this.on(events, function() {
                    return _this.__validation__setDependsOn(name, opts, arguments);
                  });
                };
              })(this))(name, opts);
            }
            value = this.get(name);
            this.__validation__presetAttr(this.attributes, opts, name, value);
            this.__validation__onSetAttr(name, value, opts);
          }
          return this;
        },
        __validation__onSetAttr: function(name, value, opts) {
          opts || (opts = this.validation[name] || {});
          if (!opts.onSet) {
            return;
          }
          return opts.onSet.call(this, value, name);
        },
        __validation__onSetAttrs: function(attrs) {
          var name, value;
          for (name in attrs) {
            value = attrs[name];
            this.__validation__onSetAttr(name, value);
          }
          return this;
        },
        __validation__setDependsOn: function(name, opts, args) {
          if (args == null) {
            args = [];
          }
          return this.set(name, opts.set.call(this, args));
        },
        __validation__presetAttr: function(attrs, opts, name, value, key) {
          if (key == null) {
            key = '';
          }
          if (!opts || opts.preset === false) {
            return this;
          }
          if (opts && opts.preset) {
            setDeep(attrs, name, opts.preset.call(this, value));
          } else if (_.isObject(value) && !_.isArray(value)) {
            setDeep(attrs, name, this.__validation__preset(value, opts, key + name + '.'));
          }
          return this;
        },
        __validation__preset: function(attrs, opts, key) {
          var name, value;
          if (key == null) {
            key = '';
          }
          if (opts && opts.preset === false) {
            return attrs;
          }
          for (name in attrs) {
            value = attrs[name];
            this.__validation__presetAttr(attrs, this.validation[key + name], name, value, key);
          }
          return attrs;
        },
        preset: function(key, val) {
          var attrs, k, v;
          if (typeof key === 'object') {
            attrs = key;
          } else {
            (attrs = {})[key] = val;
          }
          if (this.validation) {
            for (k in attrs) {
              v = attrs[k];
              this.__validation__presetAttr(attrs, this.validation[k], k, v);
            }
          }
          return attrs;
        }
      };
    })(Mexted.Extensions);
  
  }).call(this);
  
  (function() {
    var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
      hasProp = {}.hasOwnProperty;
  
    Backbone.Model.prototype._setAttribute = function(attrs, attr, val, unset) {
      if (unset) {
        return delete attrs[attr];
      } else {
        return attrs[attr] = val;
      }
    };
  
    Backbone.Model.prototype.set = function(key, val, options) {
      var attr, attrs, changed, changes, changing, current, i, len, prev, silent, unset, v;
      if (key === null) {
        return this;
      }
      if (typeof key === 'object') {
        attrs = key;
        options = val;
      } else {
        (attrs = {})[key] = val;
      }
      options || (options = {});
      if (!this._validate(attrs, options)) {
        return false;
      }
      unset = options.unset;
      silent = options.silent;
      changes = [];
      changing = this._changing;
      this._changing = true;
      if (!changing) {
        this._previousAttributes = _.clone(this.attributes);
        this.changed = {};
      }
      current = this.attributes;
      changed = this.changed;
      prev = this._previousAttributes;
      for (attr in attrs) {
        val = attrs[attr];
        if (!_.isEqual(current[attr], val)) {
          changes.push(attr);
        }
        if (!_.isEqual(prev[attr], val)) {
          changed[attr] = val;
        } else {
          delete changed[attr];
        }
        this._setAttribute(current, attr, val, unset);
      }
      if (attrs.hasOwnProperty(this.idAttribute)) {
        this.id = this.get(this.idAttribute);
      }
      if (!silent) {
        if (changes.length) {
          this._pending = options;
        }
        for (i = 0, len = changes.length; i < len; i++) {
          v = changes[i];
          this.trigger('set:' + v, this, current[v]);
          this.trigger('change:' + v, this, current[v], options);
        }
      }
      if (changing) {
        return this;
      }
      if (!silent) {
        while (this._pending) {
          options = this._pending;
          this._pending = false;
          this.trigger('change', this, options);
        }
      }
      this._pending = false;
      this._changing = false;
      return this;
    };
  
    Mexted.Model = (function(superClass) {
      extend(Model, superClass);
  
      Model.prototype.toString = function() {
        return this.escape('name');
      };
  
      Model.prototype.timedateFormat = 'L LT';
  
      function Model(attributes, options) {
        var base;
        if (options == null) {
          options = {};
        }
        base = this.initialize;
        this.initialize = function() {
          this.beforeInitExtensions.apply(this, arguments);
          this.initExtensions(options);
          return base.apply(this, arguments);
        };
        Model.__super__.constructor.call(this, attributes, options);
      }
  
      Model.prototype.beforeInitExtensions = function() {};
  
      Model.prototype.initExtensions = function(options) {
        this.initialize_nested(options);
        return this.initialize_validation(options);
      };
  
      return Model;
  
    })(Backbone.NestedModel);
  
    Mexted.extend(Mexted.Model, 'Validation');
  
    Mexted.extend(Mexted.Model, 'Nested');
  
  }).call(this);
  
  (function() {
  
  
  }).call(this);
  
  (function() {
    var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
      hasProp = {}.hasOwnProperty;
  
    Mexted.attachBindingsToView = function(View) {
      return _.defaults(View.prototype, Mexted.Bindings);
    };
  
    Mexted.BindingsGetMap = {
      'nested': function(args) {
        var name;
        name = s.camelize(args.field.slice(0, -3));
        if (args.model[name]) {
          return args.model[name].toString();
        }
      },
      'default': function(args) {
        return args.model.escape(args.field);
      }
    };
  
    Mexted.BindingsOnSetMap = {
      'toggle': function(el, args) {
        if (args.value) {
          return el.show();
        } else {
          return el.hide();
        }
      }
    };
  
    Mexted.Bindings = {
      bindings: {},
      field: function(field, tag, options) {
        var tags;
        if (tag == null) {
          tag = 'div';
        }
        if (options == null) {
          options = {};
        }
        options['data-attr'] || (options['data-attr'] = field);
        tags = options.map(function(k, v) {
          return k + "='" + v + "'";
        }).join(' ');
        return "<" + tag + " " + tags + ">" + (this.model.escape(field)) + "</" + tag + ">";
      },
      __bindings__setCommonBinding: function(field) {
        var base, binding, e, i, len, ref;
        binding = (base = this.bindings)[field] || (base[field] = {});
        if (binding.inited) {
          return;
        }
        if (binding.get !== false && (binding.get == null)) {
          binding.get = field.endsWith('_id') ? 'nested' : 'default';
        }
        if (_.isString(binding.get)) {
          binding.get = Mexted.BindingsGetMap[binding.get] || Mexted.BindingsGetMap['default'];
        }
        binding.el || (binding.el = "[data-field='" + field + "'], [data-attr='" + field + "']");
        binding.type || (binding.type = 'innerHTML');
        if (binding.onSet && _.isString(binding.onSet)) {
          binding.onSet = Mexted.BindingsOnSetMap[binding.onSet];
        }
        ref = binding.events || [];
        for (i = 0, len = ref.length; i < len; i++) {
          e = ref[i];
          this.listenTo(this.model, e, (function(_this) {
            return function(value) {
              return _this.bindingsUpdateField(field);
            };
          })(this));
        }
        binding.inited = true;
        return binding;
      },
      __bindings__setBinding: function(field, binding) {
        var b;
        b = this.__bindings[field] = _.clone(binding);
        return b.$el = this.$(b.el);
      },
      __bindings__setBindings: function() {
        var b, f, ref;
        ref = this.bindings;
        for (f in ref) {
          b = ref[f];
          this.__bindings__setBinding(f, b);
        }
        return this.bindingsShowFields(this.__bindings);
      },
      getBindingEl: function(field) {
        if (this.__bindings[field]) {
          return this.__bindings[field][$el];
        }
      },
      bindingsInit: function() {
        var f, ref, ref1, v;
        if (this.bindings === false) {
          return;
        }
        this.__bindings = {};
        ref = this.model.attributes;
        for (f in ref) {
          v = ref[f];
          this.__bindings__setCommonBinding(f);
        }
        ref1 = this.bindings;
        for (f in ref1) {
          v = ref1[f];
          this.__bindings__setCommonBinding(f);
        }
        this.listenTo(this, 'render', this.__bindings__setBindings);
        return this.listenTo(this.model, 'change', this.bindingsUpdateFields);
      },
      bindingsUpdateField: function(field, value) {
        var args, b;
        b = this.__bindings[field];
        if (b) {
          args = {
            $el: b.$el,
            model: this.model,
            field: field,
            value: value || this.model.get(field)
          };
          if (b.get && b.$el.length > 0) {
            b.$el.prop(b.type, b.get.call(this, args));
          }
          if (b.onSet) {
            b.onSet.call(this, b.$el, args);
          }
        }
        return this;
      },
      bindingsShowFields: function(fields) {
        var f, v;
        for (f in fields) {
          v = fields[f];
          this.bindingsUpdateField(f);
        }
        return this;
      },
      bindingsUpdateFields: function(model, data) {
        this.triggerMethod('bindings:beforeUpdate', arguments);
        this.bindingsShowFields(model.changed, true);
        return this;
      }
    };
  
    Mexted.Behaviors.Bindings = (function(superClass) {
      extend(Bindings, superClass);
  
      Bindings.prototype.defaults = {
        attachAttributes: true
      };
  
      function Bindings() {
        Bindings.__super__.constructor.apply(this, arguments);
        this.__bindings = {};
        this.bindings = this.getOption('bindings') || this.view.bindings;
        if (this.bindings === false) {
          return;
        }
        this.bindings || (this.bindings = {});
        this.on('before:render', this.onBeforeRender);
        this.on('render', this.onRender);
        this.on('show:field', this.showField);
        this.on('show:fields', this.showFields);
        this.listenTo(this.view.model, 'change', this.onModelChange);
      }
  
      Bindings.prototype.onBeforeRender = function() {
        var f, ref, ref1, results, v;
        if (this.getOption('attachAttributes') === true) {
          ref = this.view.model.attributes;
          for (f in ref) {
            v = ref[f];
            this.initBinding(f);
          }
        }
        ref1 = this.bindings;
        results = [];
        for (f in ref1) {
          v = ref1[f];
          results.push(this.initBinding(f));
        }
        return results;
      };
  
      Bindings.prototype.initBinding = function(field) {
        var base, binding, e, i, len, ref;
        binding = (base = this.bindings)[field] || (base[field] = {});
        if (binding.inited) {
          return;
        }
        if (binding.get !== false && (binding.get == null)) {
          binding.get = field.endsWith('_id') ? 'nested' : 'default';
        }
        if (_.isString(binding.get)) {
          binding.get = Mexted.BindingsGetMap[binding.get] || Mexted.BindingsGetMap['default'];
        }
        binding.el || (binding.el = "[data-field='" + field + "'], [data-attr='" + field + "']");
        binding.type || (binding.type = 'innerHTML');
        if (binding.onSet && _.isString(binding.onSet)) {
          binding.onSet = Mexted.BindingsOnSetMap[binding.onSet];
        }
        ref = binding.events || [];
        for (i = 0, len = ref.length; i < len; i++) {
          e = ref[i];
          this.listenTo(this.view.model, e, (function(_this) {
            return function(value) {
              return _this.showField(field);
            };
          })(this));
        }
        binding.inited = true;
        return binding;
      };
  
      Bindings.prototype.setBinding = function(field, binding) {
        var b;
        b = this.__bindings[field] = _.clone(binding);
        return b.$el = this.view.$(b.el);
      };
  
      Bindings.prototype.setBindings = function() {
        var b, f, ref;
        ref = this.bindings;
        for (f in ref) {
          b = ref[f];
          this.setBinding(f, b);
        }
        return this.showFields(this.__bindings);
      };
  
      Bindings.prototype.showField = function(field, value) {
        var args, b;
        b = this.__bindings[field];
        if (b) {
          args = {
            $el: b.$el,
            model: this.view.model,
            field: field,
            value: value || this.view.model.get(field)
          };
          if (b.get && b.$el.length > 0) {
            b.$el.prop(b.type, b.get.call(this.view, args));
          }
          if (b.onSet) {
            b.onSet.call(this.view, b.$el, args);
          }
        }
        return this;
      };
  
      Bindings.prototype.showFields = function(fields) {
        var f, v;
        for (f in fields) {
          v = fields[f];
          this.showField(f);
        }
        return this;
      };
  
      Bindings.prototype.onModelChange = function() {
        this.view.triggerMethod('bindings:beforeUpdate', arguments);
        return this.showFields(model.changed);
      };
  
      Bindings.prototype.onRender = function() {
        return this.setBindings();
      };
  
      return Bindings;
  
    })(Marionette.Behavior);
  
  }).call(this);
  
  (function() {
    var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
      hasProp = {}.hasOwnProperty;
  
    (function(Views, Marionette) {
      Views.ItemView = (function(superClass) {
        extend(ItemView, superClass);
  
        ItemView.prototype.bindings = {};
  
        ItemView.prototype.attributes = function() {
          return {
            name: this.model.get('id') || 'new',
            cid: this.model.cid
          };
        };
  
        function ItemView(options) {
          var base, base1;
          this.behaviors || (this.behaviors = {});
          (base = this.behaviors).TriggeredItem || (base.TriggeredItem = {});
          (base1 = this.behaviors).Bindings || (base1.Bindings = {});
          ItemView.__super__.constructor.apply(this, arguments);
        }
  
        ItemView.prototype.onBindingsBeforeUpdate = function() {
          return this.showFading();
        };
  
        ItemView.prototype.showFading = function(className) {
          if (className == null) {
            className = 'updated';
          }
          this.$el.removeClass(className).addClass(className);
          return this.$el.on('webkitAnimationEnd oanimationend msAnimationEnd animationend', (function(_this) {
            return function() {
              return _this.$el.removeClass(className);
            };
          })(this));
        };
  
        ItemView.prototype.actions = function(options, actions) {
          var ref;
          if (options == null) {
            options = {};
          }
          if (actions == null) {
            actions = [];
          }
          if (!options.hasOwnProperty('wrapper') && this.tagName === 'tr') {
            options.wrapper = 'td';
          }
          return Helpers.actions(this.model, options, _.union(actions, ((ref = typeof this.viewActions === "function" ? this.viewActions() : void 0) != null ? ref : this.viewActions) || []));
        };
  
        return ItemView;
  
      })(Marionette.ItemView);
      return Mexted.Views.ItemView = Views.ItemView;
    })(Mexted, Marionette);
  
  }).call(this);
  

  return Mexted;
}));